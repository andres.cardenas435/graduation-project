const departamentos = require('./Util/departamentos.json');
let municipio = require('./Util/municipios.json');
const getNextScreen = async (decryptedBody) => {
    let { screen, data, version, action, flow_token } = decryptedBody;

    if (action === "ping") {
        return {
            version,
            data: {
                status: "active",
            },
        };
    }

    if (action == "INIT") {

        return {
            version,
            screen: "INFO_STAFF",
            data: {
                departamentos,
                municipio: [{ id: "prueba", title: "prueba" }],
                visible: false,
                is_visible: false
            }
        }
    }

    if (data.departamento && !data.municipio) {
        let ciudad = municipio.filter(id => id.departamento == data.departamento);

        let municipios = [];
        for (let i = 0; i < ciudad.length; i++) {
            municipios.push({ id: ciudad[i].municipio, title: ciudad[i].municipio })
        }
        return {
            version,
            screen: "INFO_STAFF",
            data: {
                municipio: municipios,
                visible: true
            }
        }
    }
    if (data.municipio && !data.numberdoc) return {
        version,
        screen: "INFO_STAFF",
        data: {
            is_visible: true
        }
    }

    return {
        version,
        screen: "SUCCESS",
        data: {
            "extension_message_response": {
                "params": {
                    "flow_token": "<FLOW_TOKEN>",
                    "optional_param1": "<value1>",
                    "optional_param2": "<value2>"
                }
            }
        }
    }
};

module.exports = { getNextScreen };