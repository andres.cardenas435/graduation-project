const {decryptRequest,encryptResponse} = require('../Services/encryption');
const {getNextScreen} = require('../Services/flow');
const dotenv = require('dotenv');
dotenv.config();
const PRIVATE_KEY = process.env.PRIVATE_KEY;
const PASSPHRASE = ""
const health = async (req, res) => {

    try {

        const body = req.body;
        console.log({body})
        const { decryptedBody, aesKeyBuffer, initialVectorBuffer } = await decryptRequest(
            body,
            PRIVATE_KEY,
            PASSPHRASE
        );
        console.log({ decryptedBody });
        const screenResponse = await getNextScreen(decryptedBody);

        return res.send(encryptResponse(screenResponse, aesKeyBuffer, initialVectorBuffer));

    } catch (error) {
        console.log({ error });
        return res.status(200).json({
            "version": "1.0",
            "valid": true,
            "message": "Endpoint válido"
        });
    }
}

module.exports = {health};