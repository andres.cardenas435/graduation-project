const dotenv = require('dotenv');
const express = require('express');
const {health}= require('./Controller/flowController');
dotenv.config();

const app = express();
app.use(express.json({ limit: '80mb' }));

app.get('/', (req, res) => {
    res.json({ message: '¡Está funcionando!' });
});

app.get('/flow', health);
app.post('/flow', health);

const PORT = process.env.PORT || 3020;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));